include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-cce.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

dependencies {
  paths = ["../01-vpc", "../02-security", "../03-jumpbox", "../04-buckets"]
}

dependency "vpc" {
  config_path = "../01-vpc"
  mock_outputs = {
    vpc_id      = ""
    network_ids = ["", ""]
    project_id = ""
  }
  mock_outputs_merge_with_state = true
}

inputs = {
  obs_project_id = dependency.vpc.outputs.project_id
  vpc_id         = dependency.vpc.outputs.vpc_id
  network_id     = dependency.vpc.outputs.network_ids[1]
}