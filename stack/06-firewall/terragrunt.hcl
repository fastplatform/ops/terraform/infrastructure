include {
  path = find_in_parent_folders()
}
terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-acl.git?ref=v1.0.5"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

dependencies {
  paths = ["../01-vpc", "../02-security", "../03-jumpbox", "../04-buckets", "../05-k8s-cluster/"]
}

dependency "vpc" {
  config_path = "../01-vpc"
  mock_outputs = {
    port_ids = tolist([
      "",
    ])
  }
  mock_outputs_merge_with_state = true
}

dependency "k8s" {
  config_path = "../05-k8s-cluster"
  mock_outputs = {
    internal_ip = ""
  }
  mock_outputs_merge_with_state = true
}

inputs = {
  int_firewall_group_ports = [dependency.vpc.outputs.port_ids[0]]
  allow_ingress_rules_list = [
    {
      allow_ingress_rule_name             = "allow-controller-http-elb-sub-1"
      allow_ingress_rule_description      = "Allow LB HTTP traffic"
      allow_ingress_rule_action           = "allow"
      allow_ingress_rule_protocol         = "tcp"
      allow_ingress_rule_source_port      = "1:65535"
      allow_ingress_rule_destination_port = "30080"
      allow_ingress_rule_source_ip        = "100.125.0.0/16"
      allow_ingress_rule_destination_ip   = "192.168.10.0/24"
      allow_ingress_rule_enabled          = "true"
    },
    {
      allow_ingress_rule_name             = "allow-controller-http-elb-sub-2"
      allow_ingress_rule_description      = "Allow LB HTTP traffic"
      allow_ingress_rule_action           = "allow"
      allow_ingress_rule_protocol         = "tcp"
      allow_ingress_rule_source_port      = "1:65535"
      allow_ingress_rule_destination_port = "30080"
      allow_ingress_rule_source_ip        = "100.126.0.0/16"
      allow_ingress_rule_destination_ip   = "192.168.10.0/24"
      allow_ingress_rule_enabled          = "true"
    },
    {
      allow_ingress_rule_name             = "allow-gateway-https-elb-sub-1"
      allow_ingress_rule_description      = "Allow LB HTTPS traffic"
      allow_ingress_rule_action           = "allow"
      allow_ingress_rule_protocol         = "tcp"
      allow_ingress_rule_source_port      = "1:65535"
      allow_ingress_rule_destination_port = "30443"
      allow_ingress_rule_source_ip        = "100.125.0.0/16"
      allow_ingress_rule_destination_ip   = "192.168.10.0/24"
      allow_ingress_rule_enabled          = "true"
    },
    {
      allow_ingress_rule_name             = "allow-gateway-https-elb-sub-2"
      allow_ingress_rule_description      = "Allow LB HTTPS traffic"
      allow_ingress_rule_action           = "allow"
      allow_ingress_rule_protocol         = "tcp"
      allow_ingress_rule_source_port      = "1:65535"
      allow_ingress_rule_destination_port = "30443"
      allow_ingress_rule_source_ip        = "100.126.0.0/16"
      allow_ingress_rule_destination_ip   = "192.168.10.0/24"
      allow_ingress_rule_enabled          = "true"
    },
    {
      allow_ingress_rule_name             = "allow-k8s-api-elb"
      allow_ingress_rule_description      = "Allow kubernetes api traffic"
      allow_ingress_rule_action           = "allow"
      allow_ingress_rule_protocol         = "tcp"
      allow_ingress_rule_source_port      = "1:65535"
      allow_ingress_rule_destination_port = "5443"
      allow_ingress_rule_source_ip        = "0.0.0.0/0"
      allow_ingress_rule_destination_ip   = dependency.k8s.outputs.internal_ip
      allow_ingress_rule_enabled          = "true"
    }
  ]
}