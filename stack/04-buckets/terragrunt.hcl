include {
  path = find_in_parent_folders()
}
terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-obs.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

dependencies {
  paths = ["../01-vpc", "../02-security", "../03-jumpbox"]
}

dependency "vpc" {
  config_path = "../01-vpc"
  mock_outputs = {
    project_id  = ""
  }
}

dependency "bootstrap_state" {
  config_path = "../00-outputs"
  mock_outputs = {
    user_backup_id  = ""
    user_tech_id = ""
  }
}

inputs = {
  #private_kms_key_id = [dependency.bootstrap_state.outputs.private_bucket_kms_key]
  #backup_kms_key_id = [dependency.bootstrap_state.outputs.backup_bucket_kms_key]
  backup_policy  = {"Version":"2008-10-17","Id":"Policy1634202154324","Statement":[{"Sid":"SpReadWrite1634128216150","Effect":"Allow","Principal":{"AWS":["arn:aws:iam::${dependency.vpc.outputs.project_id}:user/${dependency.bootstrap_state.outputs.user_backup_id}"]},"Action":["s3:GetObject","s3:PutObject","s3:GetObjectVersion","s3:DeleteObjectVersion","s3:DeleteObject"],"Resource":["arn:aws:s3:::BUCKETNAMEPLACEHOLDER/*"]},{"Sid":"Customized1634202154145","Effect":"Allow","Principal":{"AWS":["arn:aws:iam::${dependency.vpc.outputs.project_id}:user/${dependency.bootstrap_state.outputs.user_backup_id}"]},"Action":["s3:ListBucket"],"Resource":["arn:aws:s3:::BUCKETNAMEPLACEHOLDER"]}]}
  public_policy  = { "Version" : "2008-10-17", "Id" : "Policy1611094149547", "Statement" : [{ "Sid" : "SpReadWrite1611094149487", "Effect" : "Allow", "Principal" : { "AWS" : ["arn:aws:iam::${dependency.vpc.outputs.project_id}:user/${dependency.bootstrap_state.outputs.user_tech_id}"] }, "Action" : ["s3:GetObject", "s3:PutObject", "s3:GetObjectVersion", "s3:DeleteObjectVersion", "s3:DeleteObject"], "Resource" : ["arn:aws:s3:::BUCKETNAMEPLACEHOLDER/*"] }, { "Sid" : "publicReadOnlyPolicyStatementId", "Effect" : "Allow", "Principal" : { "AWS" : ["*"] }, "Action" : ["s3:GetObject", "s3:GetObjectVersion"], "Resource" : ["arn:aws:s3:::BUCKETNAMEPLACEHOLDER/*"] }, { "Sid" : "publicHeadBucketPolicyStatementId", "Effect" : "Allow", "Principal" : { "AWS" : ["*"] }, "Action" : ["s3:HeadBucket", "s3:ListBucket"], "Resource" : ["arn:aws:s3:::BUCKETNAMEPLACEHOLDER"] }] }
  private_policy = {"Version":"2008-10-17","Id":"Policy1634196468747","Statement":[{"Sid":"SpReadWrite1634128199426","Effect":"Allow","Principal":{"AWS":["arn:aws:iam::${dependency.vpc.outputs.project_id}:user/${dependency.bootstrap_state.outputs.user_tech_id}"]},"Action":["s3:GetObject","s3:PutObject","s3:GetObjectVersion","s3:DeleteObjectVersion","s3:DeleteObject"],"Resource":["arn:aws:s3:::BUCKETNAMEPLACEHOLDER/*"]},{"Sid":"Customized1634196468634","Effect":"Allow","Principal":{"AWS":["arn:aws:iam::${dependency.vpc.outputs.project_id}:user/${dependency.bootstrap_state.outputs.user_tech_id}"]},"Action":["s3:ListBucket"],"Resource":["arn:aws:s3:::BUCKETNAMEPLACEHOLDER"]}]}
}