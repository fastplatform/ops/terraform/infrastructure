include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-jumpbox.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

dependencies {
  paths = ["../01-vpc", "../02-security"]
}

dependency "bootstrap_state" {
  config_path = "../00-outputs"
  mock_outputs = {
    fixed_eip_address  = ""
  }
}

dependency "vpc" {
  config_path = "../01-vpc"
  mock_outputs = {
    subnet_ids  = [""]
    network_ids = [""]
  }
}

dependency "security" {
  config_path = "../02-security"
  mock_outputs = {
    id = ""
  }
}

inputs = {
  security_groups = [dependency.security.outputs.id]
  subnet_id       = dependency.vpc.outputs.subnet_ids[0]
  network_id      = dependency.vpc.outputs.network_ids[0]
  jumpbox_eip = dependency.bootstrap_state.outputs.fixed_eip_address[1]
}