include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-cce-k8s-manifests.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

generate "kubeconfig" {
  path      = "config"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
{
    "kind": "Config",
    "apiVersion": "v1",
    "preferences": {},
    "clusters": [
        {
            "name": "internalCluster",
            "cluster": {
                "server": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 0), "server", "null")}",
                "certificate-authority-data": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 0), "certificate_authority_data", "null")}"
            }
        },
        {
            "name": "externalCluster",
            "cluster": {
                "server": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 2), "server", "null")}",
                "certificate-authority-data": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 2), "certificate_authority_data", "null")}"
            }
        }
    ],
    "users": [
        {
            "name": "user",
            "user": {
                "client-certificate-data": "${lookup(element(dependency.k8s.outputs.certificate_users, 0), "client_certificate_data", "null")}",
                "client-key-data": "${lookup(element(dependency.k8s.outputs.certificate_users, 0), "client_key_data", "null")}"
            }
        }
    ],
    "contexts": [
        {
            "name": "internal",
            "context": {
                "cluster": "internalCluster",
                "user": "user"
            }
        },
        {
            "name": "external",
            "context": {
                "cluster": "externalCluster",
                "user": "user"
            }
        }
    ],
    "current-context": "external"
}
EOF
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF

terraform {
  required_providers {
    kustomization = {
      source = "kbst/kustomization"
      version = "0.5.0"
    }
  }
}

provider "kustomization" {
    kubeconfig_path = "config"
}
EOF
}

dependencies {
  paths = ["../01-vpc", "../02-security", "../03-jumpbox", "../04-buckets", "../05-k8s-cluster/", "../06-firewall/"]
}

dependency "k8s" {
  config_path = "../05-k8s-cluster"
  mock_outputs = {
    node_pool_id = [""]
  }
  mock_outputs_merge_with_state = true
}

inputs = {
  manifests_path = "manifests"
  node_pool_ids = [dependency.k8s.outputs.node_pool_id]
}
