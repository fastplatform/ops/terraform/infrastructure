include {
  path = find_in_parent_folders()
}
terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-security-group.git?ref=v1.0.2"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

dependencies {
  paths = ["../01-vpc"]
}

inputs = {}
