include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/flux2.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

generate "kubeconfig" {
  path      = "config"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
{
    "kind": "Config",
    "apiVersion": "v1",
    "preferences": {},
    "clusters": [
        {
            "name": "internalCluster",
            "cluster": {
                "server": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 0), "server", "null")}",
                "certificate-authority-data": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 0), "certificate_authority_data", "null")}"
            }
        },
        {
            "name": "externalCluster",
            "cluster": {
                "server": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 1), "server", "null")}",
                "certificate-authority-data": "${lookup(element(dependency.k8s.outputs.certificate_clusters, 1), "certificate_authority_data", "null")}"
            }
        }
    ],
    "users": [
        {
            "name": "user",
            "user": {
                "client-certificate-data": "${lookup(element(dependency.k8s.outputs.certificate_users, 0), "client_certificate_data", "null")}",
                "client-key-data": "${lookup(element(dependency.k8s.outputs.certificate_users, 0), "client_key_data", "null")}"
            }
        }
    ],
    "contexts": [
        {
            "name": "internal",
            "context": {
                "cluster": "internalCluster",
                "user": "user"
            }
        },
        {
            "name": "external",
            "context": {
                "cluster": "externalCluster",
                "user": "user"
            }
        }
    ],
    "current-context": "external"
}
EOF
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  required_version = ">= 0.13"

  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.2"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.10.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = ">= 0.0.13"
    }
  }
}

provider "flux" {}

provider "kubectl" {
  config_path = "config"
}

provider "kubernetes" {
  config_path = "config"
}

EOF
}

dependencies {
  paths = ["../01-vpc", "../02-security", "../03-jumpbox", "../04-buckets", "../05-k8s-cluster/", "../06-firewall/", "../07-k8s-manifests/"]
}

dependency "k8s" {
  config_path = "../05-k8s-cluster"
    mock_outputs = {
    certificate_clusters = ""
    certificate_users = ""

  }
  mock_outputs_merge_with_state = true
}


dependency "vpc" {
  config_path = "../01-vpc"
    mock_outputs = {
    knative_ingress_gateway_elb_id = ""
    knative_ingress_gateway_elb_subnet_id = ""

  }
  mock_outputs_merge_with_state = true
}

inputs = {
  knative_ingress_gateway_elb_id = dependency.vpc.outputs.elb_id
  knative_ingress_gateway_elb_subnet_id = dependency.vpc.outputs.subnet_ids[1]
}