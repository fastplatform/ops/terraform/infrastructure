include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-vpc.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
variable "os_region" {
description = "Flexible engine region" 
}
variable "os_access_key" {
description = "Flexible engine user AK"
}
variable "os_secret_key" {
description = "Flexible engine user SK"
}
variable "os_tenant_name" {
description = "Flexible engine tenant name"
}
variable "os_domain_name" {
description = "Flexible engine domain name"
}
variable "os_auth_url" {
description = "Flexible engine url endpoint"
}
variable "os_user_name" {
description = "Flexible engine username"
}
variable "os_password" {
description = "Flexible engine user password"
}

terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
    flexibleengine = {
      source = "FlexibleEngineCloud/flexibleengine"
    }
  }
}

provider "openstack" {
  user_name   = var.os_user_name
  tenant_name = var.os_tenant_name
  password    = var.os_password
  auth_url    = var.os_auth_url
  domain_name   = var.os_domain_name
  region      = var.os_region
}

provider "flexibleengine" {
    region        = var.os_region
    access_key    = var.os_access_key
    secret_key    = var.os_secret_key
    user_name     = var.os_user_name
    password      = var.os_password
    tenant_name   = var.os_tenant_name
    domain_name   = var.os_domain_name
    auth_url      = var.os_auth_url
}
EOF
}

dependency "bootstrap_state" {
  config_path = "../00-outputs"
  mock_outputs = {
    fixed_eip_address  = ""
  }
}

inputs = {
  elb_eip = dependency.bootstrap_state.outputs.fixed_eip_address[0]
}