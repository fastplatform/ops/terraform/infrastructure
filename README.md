# Infrastructure

This repository is a template containing all the materials to provision and manage the infrastructure of the FaST Platform on Flexible Engine (Orange Business Services).

[Terraform](https://github.com/hashicorp/terraform), [Terragrunt](https://github.com/gruntwork-io/terragrunt) and [Atlantis](https://www.runatlantis.io/) are used to manage the code of the infrastructure. The Terraform state is stored in a S3 Bucket on Flexible Engine. Detailed information is provided in this [document](https://gitlab.com/fastplatform/docs/-/blob/master/reference/ci-cd.md#continuous-deployment-of-the-infrastructure-using-terraform-terragrunt-and-atlantis).

This repository is structured as follows:

```bash
infrastructure/
  ├── stack # Terraform modules
  |     ├── 00-outputs # Export all output from infrastructure-bootstrap
  |     ├── 01-vpc # Terraform module which creates VPC, subnets, NAT gateway resources and SNAT rules on Flexible Engine
  |     ├── 02-security # Terraform module which creates security groups and associated rules resources on Flexible Engine.
  |     ├── 03-jumpbox # Terraform module which creates jumpbox resource on Flexible Engine
  |     ├── 04-buckets # Terraform module which creates OBS Bucket resources on Flexible Engine cloud
  |     ├── 05-k8s-cluster # Terraform module for deploying a CCEv2 cluster
  |     ├── 06-firewall # Terraform module which creates network ACL resource on Flexible Engine
  |     ├── 07-k8s-manifest # Terraform module for deploying k8s resources on the Flexible Engine CCE cluster
  |     └── 08-flux2 # Terraform module which creates flux deployment in the Kubernetes cluster
  ├── terraform.tfvars.template # Configuration of the variables of the Terraform modules used
  └── terragrunt.hcl.template # Terragrunt configs
```

> The subgroup [modules](https://gitlab.com/fastplatform/ops/terraform/modules) contains the implementation of all the Terraform modules used.
